@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-around">
    <div class="col-md-6 ">
      <h1>Editar dato</h1>
      <form action="{{route('areas.update',$area)}}" class="form-horizontal my-4" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
          <input type="text" class="form-control" value="{{old('name',$area->name)}}" name="name" id="name"
            placeholder="Escriba el nombre">
        </div>
        <div class="form-group">
          <button class="btn btn-primary text-center btn-block">Guardar</button>
        </div>
      </form>
    </div>

  </div>
</div>

@endsection