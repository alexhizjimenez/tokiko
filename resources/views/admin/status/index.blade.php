@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-around">
    <div class="col-md-4">
      <h1>Agregar dato</h1>
      <form action="{{route('status.store')}}" class="form-horizontal my-4" method="post">
        @csrf
        <div class="form-group">
          <input type="text" class="form-control" name="name" id="name" placeholder="Escriba el nombre">
        </div>
        <div class="form-group">
          <button class="btn btn-primary text-center btn-block">Guardar</button>
        </div>
      </form>
    </div>
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Estatus') }}</div>

        <div class="card-body">
          <table id="datatable" class="table table-responsive-sm table-striped">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Acciones</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($status as $s)
              <tr>
                <td>{{ $s->name }}</td>
                <td><a href="{{route('status.edit',$s)}}">Editar</a></td>
                <td>
                  <form action="{{route('status.destroy',$s)}}" method="post"
                    onclick="return confirm('¿Desea eliminar?'); return false;">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">X</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>


        </div>
      </div>
    </div>
  </div>
</div>
@section('scripts')
<script src="{{URL::asset('js/datatable.js')}}"></script>
@endsection
@endsection