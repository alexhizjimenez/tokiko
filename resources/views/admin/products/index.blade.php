@extends('layouts.app')

@section('content')
<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      <div class="row">
        <h4 class="text-primary mx-auto">{{ __('Productos') }}</h4>
        <a href="{{route('products.create')}}" class="btn btn-primary mx-auto">Agregar</a>
      </div>
    </div>

    <div class="card-body">
      <table id="datatable" class="table table-responsive-sm table-striped">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Cantidad</th>
            <th>Area</th>
            <th>Marca</th>
            <th>Categorias</th>
            <th>Ultima modificación</th>
            <th>Estado</th>
            <th>Acciones</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($products as $p)
          <tr>
            <td>{{ $p->name }}</td>
            <td>{{ $p->description }}</td>
            <td>{{ $p->count }}</td>
            <td>{{ $p->area->name }}</td>
            <td>{{ $p->mark->name }}</td>
            <td>{{ $p->category->name }}</td>
            <td>{{ $p->updated_at}}</td>
            <td>{{ $p->statu->name }}</td>
            <td><a href="{{route('products.edit',$p)}}">Editar</a></td>
            <td>
              <form action="{{route('products.destroy',$p)}}" method="post"
                onclick="return confirm('¿Desea eliminar?'); return false;">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger">X</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>

@section('scripts')
<script src="{{URL::asset('js/datatable.js')}}"></script>
@endsection
@endsection