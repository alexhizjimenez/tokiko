@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-around">
    <div class="col-md-12 ">
      <h1>Editar dato</h1>
      <form action="{{route('products.update',$product)}}" class="form-horizontal my-4" method="post">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" value="{{old('name',$product->name)}}" name="name" id="name"
                placeholder="Escriba el nombre">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" value="{{old('description',$product->description)}}"
                name="description" id="description" placeholder="Escriba la descripcion">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="number" class="form-control" value="{{old('count',$product->count)}}" name="count" id="count"
                placeholder="Escribe la cantidad">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <select name="category_id" id="category_id" class="form-control">
                <option value="" disabled selected>Selecciona la categoria</option>
                @forelse ($categories as $c)
                <option @if ( $product->category_id == $c->id) selected : @endif value="{{$c->id}}">{{$c->name}}
                </option>
                @empty
                <option value="" disabled>No hay categorias</option>
                @endforelse
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <select name="mark_id" id="mark_id" class="form-control">
                <option value="" disabled selected>Selecciona la marca</option>
                @forelse ($marks as $m)
                <option @if ( $product->mark_id == $m->id) selected : @endif value="{{$m->id}}">{{$m->name}}</option>
                @empty
                <option value="" disabled>No hay marcas</option>
                @endforelse
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <select name="statu_id" id="statu_id" class="form-control">
                <option value="" disabled selected>Selecciona el estatus </option>
                @forelse ($status as $s)
                <option @if ( $product->statu_id == $s->id) selected : @endif value="{{$s->id}}">{{$s->name}}</option>
                @empty
                <option value="" disabled>No hay status</option>
                @endforelse
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <select name="area_id" id="area_id" class="form-control">
                <option value="" disabled selected>Selecciona el area </option>
                @forelse ($areas as $a)
                <option @if ( $product->area_id == $a->id) selected : @endif value="{{$a->id}}">{{$a->name}}</option>
                @empty
                <option value="" disabled>No hay areas</option>
                @endforelse
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <button class="btn btn-primary text-center btn-block">Guardar</button>
            </div>
          </div>
        </div>
      </form>
    </div>

  </div>
</div>
@endsection