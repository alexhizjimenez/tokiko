@if ($errors->any())

<div class="col-md-5 offset-md-4">
  <div class="alert alert-danger" role="alert">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
</div>

@endif
@if (Session::has('message') || Session::has('color'))
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="alert alert-{{Session::get('color')}}" role="alert">
      {{Session::get('message')}}
    </div>
  </div>
</div>

@endif