@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">{{ __('Users') }}</div>

        <div class="card-body">
          <table id="datatable" class="table table-responsive-sm table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($users as $user)
              <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>


        </div>
      </div>
    </div>
  </div>
</div>
@section('scripts')
<script src="{{URL::asset('js/datatable.js')}}"></script>
@endsection
@endsection