<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    $this->call([
      LaratrustSeeder::class,
      UserSeeder::class,
      AreaSeeder::class,
      CategorySeeder::class,
      MarkSeeder::class,
      StatuSeeder::class,
      ProductSeeder::class,
    ]);
  }
}
