<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Product::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    return [
      'name' => $this->faker->name,
      'description' => $this->faker->text(80),
      'count' => rand(5, 20),
      'category_id' => rand(1, 10),
      'area_id' => rand(1, 10),
      'mark_id' => rand(1, 10),
      'statu_id' => rand(1, 3),
    ];
  }
}
