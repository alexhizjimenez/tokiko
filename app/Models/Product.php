<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
  use HasFactory;
  protected $guarded = [];

  public function area(): BelongsTo
  {
    return $this->belongsTo(Area::class);
  }
  public function category(): BelongsTo
  {
    return $this->belongsTo(Category::class);
  }
  public function mark(): BelongsTo
  {
    return $this->belongsTo(Mark::class);
  }
  public function Statu(): BelongsTo
  {
    return $this->belongsTo(Statu::class);
  }
}
