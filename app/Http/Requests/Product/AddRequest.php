<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }


  public function rules()
  {
    return [
      'name' => 'required|unique:products,name',
      'description' => 'required',
      'count' => 'required|integer',
      'category_id' => 'required',
      'mark_id' => 'required',
      'statu_id' => 'required',
      'area_id' => 'required',
    ];
  }
  public function messages()
  {
    return [
      'name.required' => 'El nombre debe ser requerido',
      'name.unique' => 'El nombre ya existe ',
      'description.required' => 'La descripcion es requerido',
      'count.integer' => 'La cantidad debe ser entero',
      'count.required' => 'La cantidad es requerida',
      'category_id.required' => 'La categoria debe ser requerido',
      'mark_id.required' => 'La marca debe ser requerido',
      'statu_id.required' => 'El estatus debe ser requerido',
      'area_id.required' => 'El area debe ser requerido',
    ];
  }
}
