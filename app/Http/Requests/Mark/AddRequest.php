<?php

namespace App\Http\Requests\Mark;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }


  public function rules()
  {
    return [
      'name' => 'required|unique:marks,name',
    ];
  }
  public function messages()
  {
    return [
      'name.required' => 'El nombre debe ser requerido',
      'name.unique' => 'El nombre ya existe '
    ];
  }
}
