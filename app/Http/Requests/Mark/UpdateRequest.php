<?php

namespace App\Http\Requests\Mark;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }


  public function rules()
  {
    return [
      'name' => 'required|unique:marks,name,' . $this->mark->id,
    ];
  }
  public function messages()
  {
    return [
      'name.required' => 'El nombre debe ser requerido',
      'name.unique' => 'El nombre ya existe '
    ];
  }
}
