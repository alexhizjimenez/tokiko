<?php

namespace App\Http\Requests\Statu;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }


  public function rules()
  {
    dd($this->statu);
    return [
      'name' => 'required|unique:status,name,' . $this->statu,
    ];
  }
  public function messages()
  {
    return [
      'name.required' => 'El nombre debe ser requerido',
      'name.unique' => 'El nombre ya existe '
    ];
  }
}
