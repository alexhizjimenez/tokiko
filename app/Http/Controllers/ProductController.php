<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\AddRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Models\Area;
use App\Models\Category;
use App\Models\Mark;
use App\Models\Product;
use App\Models\Statu;
use Illuminate\Http\Request;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $products = Product::all();
    return view('admin.products.index', compact('products'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $categories = Category::all();
    $marks = Mark::all();
    $status = Statu::all();
    $areas = Area::all();
    return view('admin.products.add', compact(['categories', 'marks', 'status', 'areas']));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(AddRequest $request)
  {
    Product::create($request->all());
    return redirect()->route('products.index')->with(['message' => 'Agregado', 'color' => 'success']);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Product  $product
   * @return \Illuminate\Http\Response
   */
  public function show(Product $product)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Product  $product
   * @return \Illuminate\Http\Response
   */
  public function edit(Product $product)
  {
    $categories = Category::all();
    $marks = Mark::all();
    $status = Statu::all();
    $areas = Area::all();
    return view('admin.products.edit', compact(['categories', 'marks', 'status', 'areas', 'product']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Product  $product
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateRequest $request, Product $product)
  {
    Product::where('id', $product->id)->update($request->except(['_token', '_method']));
    return redirect()->route('products.index')->with(['message' => 'Editado', 'color' => 'success']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Product  $product
   * @return \Illuminate\Http\Response
   */
  public function destroy(Product $product)
  {
    $color = "danger";
    $message = "No se pudo eliminar";

    try {
      $product->delete();
      $color = "success";
      $message = "Eliminado";
    } catch (\Throwable $th) {
      //throw $th;
    }
    return redirect()->route('products.index')->with(['message' => $message, 'color' => $color]);
  }
}
