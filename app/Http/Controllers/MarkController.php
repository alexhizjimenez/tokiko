<?php

namespace App\Http\Controllers;

use App\Http\Requests\Mark\AddRequest;
use App\Http\Requests\Mark\UpdateRequest;
use App\Models\Mark;
use Illuminate\Http\Request;

class MarkController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $marks = Mark::all();
    return view('admin.marks.index', compact('marks'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(AddRequest $request)
  {
    Mark::create($request->all());
    return redirect()->route('marks.index')->with(['message' => 'Agregado', 'color' => 'success']);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Mark  $mark
   * @return \Illuminate\Http\Response
   */
  public function show(Mark $mark)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Mark  $mark
   * @return \Illuminate\Http\Response
   */
  public function edit(Mark $mark)
  {
    return view('admin.marks.edit', compact('mark'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Mark  $mark
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateRequest $request, Mark $mark)
  {
    Mark::where('id', $mark->id)->update($request->only('name'));
    return redirect()->route('marks.index')->with(['message' => 'Editado', 'color' => 'success']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Mark  $mark
   * @return \Illuminate\Http\Response
   */
  public function destroy(Mark $mark)
  {
    $color = "danger";
    $message = "No se pudo eliminar";

    try {
      $mark->delete();
      $color = "success";
      $message = "Eliminado";
    } catch (\Throwable $th) {
      //throw $th;
    }
    return redirect()->route('marks.index')->with(['message' => $message, 'color' => $color]);
  }
}
