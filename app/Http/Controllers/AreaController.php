<?php

namespace App\Http\Controllers;

use App\Http\Requests\Area\AddRequest;
use App\Http\Requests\Area\UpdateRequest;
use App\Models\Area;
use Illuminate\Http\Request;

class AreaController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $areas = Area::all();
    return view('admin.areas.index', compact('areas'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(AddRequest $request)
  {
    Area::create($request->all());
    return redirect()->route('areas.index')->with(['message' => 'Agregado', 'color' => 'success']);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Area  $area
   * @return \Illuminate\Http\Response
   */
  public function show(Area $area)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Area  $area
   * @return \Illuminate\Http\Response
   */
  public function edit(Area $area)
  {
    return view('admin.areas.edit', compact('area'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Area  $area
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateRequest $request, Area $area)
  {
    Area::where('id', $area->id)->update($request->only('name'));
    return redirect()->route('areas.index')->with(['message' => 'Editado', 'color' => 'success']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Area  $area
   * @return \Illuminate\Http\Response
   */
  public function destroy(Area $area)
  {
    $color = "danger";
    $message = "No se pudo eliminar";

    try {
      $area->delete();
      $color = "success";
      $message = "Eliminado";
    } catch (\Throwable $th) {
      //throw $th;
    }
    return redirect()->route('areas.index')->with(['message' => $message, 'color' => $color]);
  }
}
