<?php

namespace App\Http\Controllers;

use App\Http\Requests\Statu\AddRequest;
use App\Http\Requests\Statu\UpdateRequest;
use App\Models\Statu;
use Illuminate\Http\Request;

class StatuController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $status = Statu::all();
    return view('admin.status.index', compact('status'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(AddRequest $request)
  {
    Statu::create($request->all());
    return redirect()->route('status.index')->with(['message' => 'Agregado', 'color' => 'success']);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Statu  $statu
   * @return \Illuminate\Http\Response
   */
  public function show(Statu $statu)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Statu  $statu
   * @return \Illuminate\Http\Response
   */
  public function edit(Statu $statu, $id)
  {
    $statu = Statu::find($id);
    return view('admin.status.edit', compact('statu'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Statu  $statu
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $messages =
      [
        'name.required' => 'El nombre debe ser requerido',
        'name.unique' => 'El nombre ya existe '
      ];
    $request->validate([
      'name' => 'required|unique:status,name,' . $id,

    ], $messages);
    Statu::where('id', $id)->update($request->only('name'));
    return redirect()->route('status.index')->with(['message' => 'Editado', 'color' => 'success']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Statu  $statu
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $color = "danger";
    $message = "No se pudo eliminar";

    try {
      $statu  = Statu::find($id);
      $statu->delete();
      $color = "success";
      $message = "Eliminado";
    } catch (\Throwable $th) {
      //throw $th;
    }
    return redirect()->route('status.index')->with(['message' => $message, 'color' => $color]);
  }
}
