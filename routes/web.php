<?php

use App\Http\Controllers\AreaController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MarkController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\StatuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect()->route('login');
});

Auth::routes();



Route::middleware(['auth'])->group(function () {
  Route::get('/home', [HomeController::class, 'index'])->name('home');
  Route::get('users', [UserController::class, 'index'])->name('users.index');
  Route::get('profile', [ProfileController::class, 'show'])->name('profile.show');
  Route::put('profile', [ProfileController::class, 'update'])->name('profile.update');
  Route::resource('areas', AreaController::class);
  Route::resource('categories', CategoryController::class);
  Route::resource('marks', MarkController::class);
  Route::resource('status', StatuController::class);
  Route::resource('products', ProductController::class);
});
