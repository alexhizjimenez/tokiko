## Proyecto tokiko salina cruz oax.

Repositorio tokiko

### Como usarse

Clone this project to your local computer.

```ps
git clone https://github.com/Alexhiz/tokiko.git
```

Navigate to the project folder.

```ps
cd Laravel-UI-Pages-Skeleton/
```

Install required packages.

```ps
composer install
```

create new .env file and edit database credentials there.

```ps
cp .env.example .env
```

Generate new app key.

```ps
php artisan key:generate
```

Run migrations. (it has some seeded data for your testing)

```ps
php artisan migrate --seed
```

That's it: launch the main URL

---
